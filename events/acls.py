from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

def get_pexels_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    try:
        response = requests.get(url, headers=headers)
        response = response.json()
        return response
    except:
        return None    

def get_weather_data(city, state):
    url = f"https://api.openweathermap.org/data/2.5/weather?q={city},{state},1&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    try:
        response = requests.get(url, headers=headers)
        response = response.json()
        weather_data = {
            "temperature": round(response["main"]["temp"]),
            "description": response["weather"][0]["description"],
        }
        return weather_data
    except:
        return None    

   
    