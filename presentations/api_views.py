from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import Presentation, Status
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
    ]
    def get_extra_data(self, o):
        return {
            "status": o.status.name,
        }

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
       
    ]
  

    def get_extra_data(self, o):
        return {
            "status": o.status.name,
        }       

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference_id=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            status = Status.objects.get(id=content["status"])
            content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status id"},
                status=400,
            )
        content["conference_id"] = conference_id
        presentation = Presentation.objects.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])           
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = get_object_or_404(Presentation, id=id)
        presentation_data = PresentationDetailEncoder().default(presentation)
        return JsonResponse(presentation_data, safe=False)
    
    elif  request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    
        
    else:
        content = json.loads(request.body)
        try:
            status = Status.objects.get(id=content["status"])
            content["status"] = status
            conference = Conference.objects.get(id=content["conference_id"])
            content["conference"] = conference
        except Status.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status id"},
                status=400,
            ) 
        presentation = Presentation.objects.filter(id=id).update(**content)
        return JsonResponse(presentation, encoder=PresentationDetailEncoder, safe=False)        