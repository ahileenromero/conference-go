from django.http import JsonResponse, HttpResponseNotFound
from django.shortcuts import get_object_or_404
from .models import Attendee
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
from events.models import Conference
import json



class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [ 
        "name",
    ]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        "conference",
        ""
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def default(self, o):
        if isinstance(o, Attendee):
            return {
                "email": o.email,
                "name": o.name,
                "company_name": o.company_name,
                "created": o.created.isoformat() if o.created else None,
                "conference": {
                    "name": o.conference.name,
                    "href": o.conference.get_api_url(),
                } if o.conference else None,
            }
        return super().default(o)
    
@require_http_methods(["GET", "POST", "PUT"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    else:  # PUT
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        for attendee_data in content.get("attendees", []):
            attendee_id = attendee_data.get("id")
            if attendee_id:
                try:
                    attendee = Attendee.objects.get(
                        id=attendee_id, conference=conference
                    )
                    for key, value in attendee_data.items():
                        if key != "id" and hasattr(attendee, key):
                            setattr(attendee, key, value)
                    attendee.save()
                except Attendee.DoesNotExist:
                    return JsonResponse(
                        {
                            "message": f"Attendee with id {attendee_id} not found"
                        },
                        status=400,
                    )
            else:
                attendee_data["conference"] = conference
                Attendee.objects.create(**attendee_data)
        updated_attendees = Attendee.objects.filter(conference=conference)
        return JsonResponse(
            {"attendees": updated_attendees},
            encoder=AttendeeListEncoder,
        )

    
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return HttpResponseNotFound({"message": "Attendee not found"})
    if request.method == "GET":
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            setattr(attendee, key, value)
        attendee.save()
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        attendee.delete()
        return JsonResponse({"message": "Attendee deleted"})
    